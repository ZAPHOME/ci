# Setup System
FROM ubuntu

# Setup workspace
ENV ZAPHOME_HOME=/zaphome
RUN mkdir $ZAPHOME_HOME
WORKDIR $ZAPHOME_HOME

# Add zaphome bins
ADD bin $ZAPHOME_HOME/bin
RUN chmod a+x $ZAPHOME_HOME/bin/central

# Run zaphome
ENTRYPOINT cd $ZAPHOME_HOME/bin && ./central
